export interface IQuery {
	name: string;
	phrase: string;
	default: boolean;
}

export interface IFilter {
	id: number;
	name: string;
	queries: IQuery[];
}

export interface IGroup {
	id: number;
	name: string;
	filters: IFilter[];
	filterData: IFilter;
}

export interface IMaterial {
	cena: number;
	d1: string;
	d2: string;
	d3: string;
	d4: string;
	elem_id: number;
	enabled: boolean;
	grupa: string;
	id: number;
	jm: string;
	jr: string;
	kod: string;
	masa: number;
	nazwa: string;
	photo: string;
	rodzaj: string;
	url: string;
	us: number;
	x: number;
	y: number;
	z: number;
}
