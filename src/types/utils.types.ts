export interface IResponseMetadata {
	currentPage: number;
	itemCount: number;
	itemsPerPage: number;
	totalItems: number;
	totalPages: number;
}
