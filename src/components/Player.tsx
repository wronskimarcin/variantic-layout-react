import { useContext, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { PlayerContext } from "../contexts/PlayerContext";
import {
	getUnit,
	setCategories,
	setParameters,
	setPrice,
	setUnit,
} from "../redux/configurationSlice";
import { useAppDispatch } from "../redux/hooks";
import { setGroups, setSelectedMaterials } from "../redux/materialsSlice";
import { setPlayerLoaded } from "../redux/modesSlice";

interface IPlayerProps {
	id: string;
	langCode: string;
	documentationTitle: string;
	configurationId: string;
	dataToLoad: string;
	arProps: Record<string, string>;
}

const Player: React.FC<IPlayerProps> = ({
	id,
	langCode,
	documentationTitle,
	configurationId,
	dataToLoad,
	arProps,
}) => {
	const dispatch = useAppDispatch();
	const playerContext = useContext(PlayerContext);
	const playerRef = useRef<any>(null);
	const selectedUnit = useSelector(getUnit);

	const updatePlayer = async () => {
		const playerDOM = playerRef.current;
		if (!playerDOM || !playerContext) return;

		const player = playerDOM.player;
		playerContext.player = player;
		playerContext.webComponent = playerRef.current;

		player.setSelected(player.setRoots.filterScenes()[0]);

		const { categories, params } = player.mediator.getConfigurationData();

		dispatch(setParameters(params));
		dispatch(setCategories(categories));
		dispatch(setUnit(player.targetUnit));

		const groupData = await player.setLayoutMediator.getGroupData();
		dispatch(setGroups(groupData));

		const selectedMaterials = player.setGroupsMaterial;
		dispatch(setSelectedMaterials(selectedMaterials));
	};

	const updatePrice = () => {
		const playerDOM = playerRef.current;
		if (!playerDOM) return;

		const player = playerDOM.player;
		const price = player.mediator.getSetCurrencyPrice();
		dispatch(setPrice(price));
	};

	useEffect(() => {
		if (!selectedUnit) return;

		updatePlayer();
		// eslint-disable-next-line
	}, [selectedUnit]);

	useEffect(() => {
		if (!playerRef || !playerRef.current) return;
		const playerDOM = playerRef.current;

		const initEvents = () => {
			if (!playerDOM) return;

			dispatch(setPlayerLoaded(true));
			updatePlayer();
			updatePrice();
			playerDOM.addEventListener("updatePrice", updatePrice);
			playerDOM.addEventListener("playerReloaded", updatePlayer);
			playerDOM.addEventListener("playerSceneReloaded", updatePlayer);
		};

		playerDOM.addEventListener("playerLoaded", initEvents);

		return () => {
			playerDOM.removeEventListener("playerLoaded", initEvents);
			playerDOM.addEventListener("updatePrice", updatePrice);
			playerDOM.removeEventListener("playerReloaded", updatePlayer);
			playerDOM.removeEventListener("playerSceneReloaded", updatePlayer);
		};
		// eslint-disable-next-line
	}, [playerRef, dispatch]);

	return (
		<variantic-player
			ref={playerRef}
			component-id={id}
			disable-layout
			width="100%"
			height="100%"
			lang-code={langCode}
			layout-id="-1"
			documentation-title={documentationTitle}
			configuration-id={configurationId}
			data-to-load={dataToLoad}
			{...arProps}
		></variantic-player>
	);
};

export default Player;
