import { useContext } from "react";
import { useSelector } from "react-redux";
import Layout from "./components/layout/Layout";
import Player from "./components/Player";
import PlayerLayer from "./components/PlayerLayer";
import { ColorsContext } from "./contexts/ColorsContext";
import { getPrice } from "./redux/configurationSlice";
import { useAppSelector } from "./redux/hooks";
import { getFullscreen } from "./redux/modesSlice";

interface IAppProps {
	id?: string;
	name?: string;
	langCode?: string;
	configurationId?: string;
	dataToLoad?: string;
	documentationTitle?: string;
	hideDocumentation?: boolean;
	hidePrice?: boolean;
	arProps?: Record<string, string>;
}

const App: React.FC<IAppProps> = ({
	id,
	name,
	langCode,
	configurationId,
	dataToLoad,
	documentationTitle,
	hideDocumentation,
	hidePrice,
	arProps,
}) => {
	const price = useSelector(getPrice);
	const fullscreen = useAppSelector(getFullscreen);
	const colors = useContext(ColorsContext);

	if (!id) return null;

	return (
		<div className="App tw-w-full tw-h-full tw-relative tw-overflow-hidden ">
			<div
				className={`${
					fullscreen ? "left-collapsed" : ""
				} w-full md:tw-w-2/3 lg:tw-w-3/4 tw-transition-all tw-duration-500 tw-flex tw-flex-col tw-h-3/4 md:tw-h-full`}
			>
				<div
					className="tw-flex tw-justify-between tw-p-2 tw-text-2xl tw-border-b"
					style={{ color: colors.text, borderColor: colors.lines }}
				>
					<span>{name}</span>
					{!hidePrice && <span>{price}</span>}
				</div>
				<div className="tw-flex-1 tw-relative">
					<PlayerLayer hideDocumentation={!!hideDocumentation} />
					<Player
						id={id}
						langCode={langCode ?? "en"}
						documentationTitle={documentationTitle ?? ""}
						configurationId={configurationId ?? ""}
						dataToLoad={dataToLoad ?? ""}
						arProps={arProps ?? {}}
					/>
				</div>
			</div>
			<div
				className={`${
					fullscreen ? "right-collapsed" : ""
				} tw-absolute tw-bottom-0 tw-left-0 md:tw-left-[unset] md:tw-bottom-[unset] md:tw-right-0 md:tw-top-0 tw-transition-all tw-duration-500 tw-h-1/4 tw-w-full tw-border tw-border-l-0 md:tw-h-full md:tw-w-1/3 lg:tw-w-1/4`}
				style={{ borderColor: colors.lines }}
			>
				<Layout />
			</div>
		</div>
	);
};

export default App;
