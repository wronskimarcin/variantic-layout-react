import { useContext, useState } from "react";
import { IGroup } from "../../types";
import { Collapse } from "react-collapse";
import Query from "./Query";
import { ColorsContext } from "../../contexts/ColorsContext";

interface IGroupProps {
	group: IGroup;
}

const Group: React.FC<IGroupProps> = ({ group }) => {
	const queries = group.filterData.queries;
	const [open, setOpen] = useState(false);
	const colors = useContext(ColorsContext);

	return (
		<div className="tw-py-2 tw-border-b" style={{ borderColor: colors.lines }}>
			<div
				className="tw-flex tw-justify-between tw-cursor-pointer"
				onClick={() => setOpen((prev) => !prev)}
			>
				<h2
					className={open ? "tw-font-semibold" : ""}
					style={{ color: colors.text }}
				>
					{group.name}
				</h2>
				<i className="material-icons" style={{ color: colors.text }}>
					{open ? "expand_less" : "expand_more"}
				</i>
			</div>
			<Collapse isOpened={open}>
				{queries.length > 1
					? queries
							.filter((query) => query.name !== "ALL")
							.map((query) => (
								<Query
									key={`${group.name}-${query.name}`}
									group={group}
									query={query}
								/>
							))
					: queries.map((query) => (
							<Query
								key={`${group.name}-${query.name}`}
								group={group}
								query={query}
							/>
					  ))}
			</Collapse>
		</div>
	);
};

export default Group;
