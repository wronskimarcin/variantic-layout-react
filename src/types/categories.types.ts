import { IParameter } from "./parameter.types";

export interface ICategory {
	id: number;
	name: string;
	params: string[];
	visible: boolean;
}

export interface ICategoryWithParameters extends ICategory {
    parameters: IParameter[];
}
