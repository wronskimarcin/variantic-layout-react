import { useContext } from "react";
import { useSelector } from "react-redux";
import { ColorsContext } from "../../contexts/ColorsContext";
import { PlayerContext } from "../../contexts/PlayerContext";
import { ShapesContext } from "../../contexts/ShapesContext";
import { getUnit, setUnit } from "../../redux/configurationSlice";
import { useAppDispatch } from "../../redux/hooks";

interface IUnitButton {
	label: string;
	unit: string;
	selected: boolean;
	onClick: (u: string) => void;
}

const UnitButton: React.FC<IUnitButton> = ({
	label,
	unit,
	selected,
	onClick,
}) => {
	const colors = useContext(ColorsContext);
	const shapes = useContext(ShapesContext);

	return (
		<button
			type="button"
			className={`tw-border tw-px-2`}
			style={{
				borderColor: colors.inputLines,
				color: selected ? colors.textActive : colors.text,
				background: selected ? colors.bgActive : colors.bg,
				borderRadius: shapes.buttonRadius,
			}}
			value={unit}
			onClick={() => onClick(unit)}
		>
			{label}
		</button>
	);
};

const UnitPicker: React.FC = () => {
	const dispatch = useAppDispatch();
	const playerContext = useContext(PlayerContext);
	const selectedUnit = useSelector(getUnit);
	const units: { unit: string; label: string }[] =
		playerContext && playerContext.player && playerContext.player.units;

	if (!units) return null;

	const onClick = (unit: string) => {
		if (!playerContext || !playerContext.player) return;

		playerContext.player.mediator.changeUnit(unit);
		dispatch(setUnit(unit));
	};

	return (
		<div className="tw-flex tw-justify-end tw-py-2 tw-gap-2">
			{units.map((u) => {
				return (
					<UnitButton
						key={u.unit}
						label={u.label}
						unit={u.unit}
						onClick={onClick}
						selected={u.unit === selectedUnit}
					/>
				);
			})}
		</div>
	);
};

export default UnitPicker;
