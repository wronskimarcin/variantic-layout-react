import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { PlayerContext } from "./contexts/PlayerContext";
import { ColorsContext, defaultColors } from "./contexts/ColorsContext";
import { defaultShapes, ShapesContext } from "./contexts/ShapesContext";
import {
	VarianticContext,
	defaultVariantic,
} from "./contexts/VarianticContext";
import { revertAll } from "./redux/extra";

const rootDOM = document.getElementById("variantic-layout") as HTMLElement;
let root = ReactDOM.createRoot(rootDOM);

function renderLayout() {
	const props: any = rootDOM.dataset;

	const extractProps = (properties: Record<string, string>, prefix: string) => {
		return Object.keys(properties)
			.filter((key) => key.startsWith(prefix))
			.reduce((acc: any, key: string) => {
				let newKey = key.replace(prefix, "");
				newKey = newKey.charAt(0).toLowerCase() + newKey.slice(1);

				acc[newKey] = props[key];
				delete props[key];
				return acc;
			}, {});
	};

	const colorsProps = extractProps(props, "color");
	const colors = {
		...defaultColors,
		...colorsProps,
	};

	const shapesProps = extractProps(props, "shape");
	const shapes = {
		...defaultShapes,
		...shapesProps,
	};

	const varianticProps = extractProps(props, "variantic");
	const variantic = {
		...defaultVariantic,
		...varianticProps,
	};

	const arProps = rootDOM
		.getAttributeNames()
		.filter((attr) => attr.startsWith("data-ar-"))
		.reduce(
			(acc, curr) => ({
				...acc,
				[curr.replace(/^data-/g, "")]: rootDOM.getAttribute(curr),
			}),
			{}
		);

	root.render(
		<React.StrictMode>
			<Provider store={store}>
				<PlayerContext.Provider
					value={{
						player: null,
						webComponent: null,
					}}
				>
					<ShapesContext.Provider value={shapes}>
						<ColorsContext.Provider value={colors}>
							<VarianticContext.Provider value={variantic}>
								<App {...props} arProps={arProps} />
							</VarianticContext.Provider>
						</ColorsContext.Provider>
					</ShapesContext.Provider>
				</PlayerContext.Provider>
			</Provider>
		</React.StrictMode>
	);
}

renderLayout();

const observedAttributes = ["data-configuration-id", "data-to-load"];

new MutationObserver((mutationList) => {
	if (
		mutationList.some(
			(mutation) =>
				mutation.type === "attributes" &&
				observedAttributes.includes(mutation.attributeName ?? "")
		) === false
	)
		return;

	store.dispatch(revertAll());

	root.unmount();
	root = ReactDOM.createRoot(rootDOM);

	renderLayout();
}).observe(rootDOM, {
	attributes: true,
});
