import React, { useContext } from "react";
import { ColorsContext } from "../../../../contexts/ColorsContext";
import { PlayerContext } from "../../../../contexts/PlayerContext";
import { ShapesContext } from "../../../../contexts/ShapesContext";
import { IParameterProps } from "../Parameter";
import { PredefinedButton } from "./Predefined";

interface IOptionsProps extends IParameterProps {
	onClick: (value: string) => void;
	visibilities: Record<number | string, boolean>;
}

const ListOptions: React.FC<IOptionsProps> = ({
	visibilities,
	parameter,
	onClick,
}) => {
	const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		const value = e.target.value;
		onClick(value);
	};
	const colors = useContext(ColorsContext);
	const shapes = useContext(ShapesContext);

	return (
		<div className="tw-flex">
			<select
				className="tw-w-full tw-border tw-px-2 tw-py-1"
				value={parameter.value}
				onChange={onChange}
				style={{
					color: colors.text,
					borderColor: colors.inputLines,
					borderRadius: shapes.selectRadius,
				}}
			>
				{Object.keys(parameter.settings.options).map((key, i) => {
					const value = key;
					if (!visibilities[value]) return null;

					const label = parameter.settings.options[key];

					return (
						<option key={`${parameter.code}-${value}-i`} value={value}>
							{label}
						</option>
					);
				})}
			</select>
		</div>
	);
};

const ButtonOptions: React.FC<IOptionsProps> = ({
	visibilities,
	parameter,
	onClick,
}) => {
	return (
		<div className="tw-flex tw-flex-wrap tw-gap-3">
			{Object.keys(parameter.settings.options).map((key, i) => {
				const value = key;
				if (!visibilities[value]) return null;

				const label = parameter.settings.options[key];
				const selected = String(parameter.value) === String(value);

				return (
					<PredefinedButton
						key={`${parameter.code}-${value}-${i}`}
						label={label}
						value={value}
						selected={selected}
						onClick={onClick}
					/>
				);
			})}
		</div>
	);
};

const Options: React.FC<IParameterProps> = ({ parameter }) => {
	const OptionsComp =
		parameter.settings.listType === "1" ? ButtonOptions : ListOptions;
	const playerContext = useContext(PlayerContext);
	const colors = useContext(ColorsContext);

	const visibilities =
		playerContext.player.mediator.getListOptionsVisibilities(parameter);

	const onClick = (value: string) => {
		if (!playerContext) return;

		const player = playerContext.player;
		if (!player) return;

		player.setSelected(player.setRoots.filterScenes()[0]);
		player.mediator.changeParameter(parameter.code, value);
	};

	return (
		<div
			className="Parameter tw-flex tw-flex-col tw-py-4 tw-border-b"
			style={{ color: colors.text, borderColor: colors.lines }}
		>
			<h3 className="tw-text-sm tw-uppercase tw-mb-2 tw-break-all">
				{parameter.name}
			</h3>
			<span className="tw-text-sm">
				<OptionsComp
					visibilities={visibilities}
					parameter={parameter}
					onClick={onClick}
				/>
			</span>
		</div>
	);
};

export default Options;
