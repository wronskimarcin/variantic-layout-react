# Variantic Layout w/ React

## Usage

### Basic

In order to use variantic layout you need to put in `head` tag blocks listed below

```html
<head>
	...
	<script defer="defer" src="/static/js/main.[hash].js"></script>
	<link href="/static/css/main.[hash].css" rel="stylesheet" />
</head>
```

Then somewhere in `body` tag put `div` tag like so:

```html
<div id="variantic-layout" data-id="580"></div>
```

**Warning**: _id_ and _data-id_ properties are obligatory!

Do not forget to add script for `variantic-player`! There are [the docs](https://variantic.atlassian.net/servicedesk/customer/portal/3/article/225574916) where you can learn how to do that (only **Script** section, rest layout will do for you).

### Extra properties

There are few extra properties that you can add to `div` tag:

- `data-name="Name of your product"` - setting the title to "Name of your product",
- `data-lang-code="pl"` - setting the language to Polish (default: en),
- `data-to-load="{ ... }"` - configuration to load (default: empty),
- `data-configuration-id="someHash"` - inject configuration with given id (default: empty) (*Not implemented - it is expected to be added in future releases*)
- `data-hide-documentation="1"` - disabling documentation download icon (default: 0),
- `data-hide-price="1"` - hide price content element (default: 0).

**Example**:

```html
<div
	id="variantic-layout"
	data-name="Inside door"
	data-id="580"
	data-lang-code="en"
></div>
```

Additionaly, you can modify AR parameters by passing attributes using `data-ar-` prefix.

**Example**:
```html
<div
	id="variantic-layout"
	...
	data-ar-ar-placement="wall"
	data-ar-shadow-intensity="0.5"
></div>
```

List of available options can be found [here](https://modelviewer.dev/docs/).

### Theming

You can change colors of certain parts of the layout. Valid values are **HEX color codes** (f.e. #ff0000) and **keyword colors** (see [here](https://www.w3.org/wiki/CSS/Properties/color/keywords)).

- `data-color-lines` - color of border lines,
- `data-color-input-lines` - color of inputs border lines,
- `data-color-text` - color of all the text phrases in layout,
- `data-color-bg` - color of the button background,
- `data-color-text-active` - color of the the text in button in active state,
- `data-color-bg-active` - color of the the button background in active state,
- `data-color-icons` - color of the icons,
- `data-color-tooltip-text` - color of the text phrase in tooltip (tooltip - window that appears on icon hover),
- `data-color-tooltip-bg` - color of the tooltip background,
- `data-color-range-thumb-bg` - color of the input of type range thumb background,
- `data-color-scroll-bg` - color of scroll background,
- `data-color-scroll-thumb-bg` - color of scrollbar thumb background.

**Example**:

```html
<div
	id="variantic-layout"
	data-name="Inside door"
	data-id="580"
	data-color-lines="#ff0000"
	data-color-text="#00ff00"
	data-color-input-lines="orange"
	data-color-text-active="yellow"
	data-color-bg="yellow"
	data-color-bg-active="cyan"
	data-color-icons="#0000ff"
	data-color-tooltip-text="black"
	data-color-tooltip-bg="transparent"
	data-color-range-thumb-bg="purple"
	data-color-range-thumb-border="darkblue"
></div>
```

### Shapes

You can also manipulate how certain shapes look in the layout. Valid values are CSS units listed [here](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#numbers_lengths_and_percentages).

- `data-shape-button-radius` - border radius of the buttons,
- `data-shape-input-radius` - border radius of the input of type number,
- `data-shape-range-radius` - border radius of the input of typ range,
- `data-shape-range-thumb-radius` - border radius of the thumb input of type range,
- `data-shape-select-radius` - border radius of the select dropdown.

**Example**:

```html
<div
	id="variantic-layout"
	data-name="Drzwi wewnętrzne"
	data-id="580"
	data-lang-code="pl"
	data-hide-documentation=""
	data-shape-button-radius="50%"
	data-shape-input-radius="50%"
	data-shape-range-radius="50%"
	data-shape-select-radius="50%"
	data-shape-range-thumb-radius="0px"
></div>
```
