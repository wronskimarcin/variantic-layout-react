import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { ColorsContext } from "../../contexts/ColorsContext";
import { getView } from "../../redux/modesSlice";
import Configurationlayout from "../configuration/ConfigurationLayout";
import MaterialsLayout from "../materials/MaterialsLayout";
import "./Layout.scss";

const viewComps: { [key: string]: React.FC } = {
	edit: Configurationlayout,
	materials: MaterialsLayout,
};

const Layout = () => {
	const view = useSelector(getView);
	const colors = useContext(ColorsContext);

	const ViewComponent = viewComps[view];

	return (
		<div
			className="Layout tw-w-full tw-h-full tw-p-4 tw-border-l tw-overflow-y-auto tw-overflow-x-hidden"
			style={
				{
					borderColor: colors.lines,
					"--scrollBg": colors.scrollBg,
					"--scrollThumbBg": colors.scrollThumbBg,
				} as any
			}
		>
			<ViewComponent />
		</div>
	);
};

export default Layout;
