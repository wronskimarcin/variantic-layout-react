import { useContext, useState } from "react";
import { useSelector } from "react-redux";
import { ColorsContext } from "../contexts/ColorsContext";
import { PlayerContext } from "../contexts/PlayerContext";
import { useAppDispatch } from "../redux/hooks";
import {
	getFullscreen,
	getModes,
	getPlayerLoaded,
	getView,
	setArLoading,
	setDocumentationLoading,
	setSnapshotLoading,
	toggleFullscreen,
	toggleView,
} from "../redux/modesSlice";

interface IPlayerLayerIconProps {
	name: string;
	styles: string;
	loading?: boolean;
	tooltipTitle?: string;
	onClick?: () => void;
}

const PlayerLayerIcon: React.FC<IPlayerLayerIconProps> = ({
	name,
	styles,
	loading,
	tooltipTitle,
	onClick,
}) => {
	const [tooltip, setTooltip] = useState(false);
	const colors = useContext(ColorsContext);

	return (
		<button
			onMouseEnter={() => setTooltip(true)}
			onMouseLeave={() => setTooltip(false)}
			className={`tw-z-50 tw-absolute ${styles}`}
			style={{}}
			type="button"
			onClick={onClick}
		>
			{tooltipTitle && (
				<span
					className={`${
						tooltip ? "" : "tw-hidden"
					} tw-absolute tw-right-0 tw-top-2 tw-px-1 tw-text-sm tw-whitespace-nowrap`}
					style={{
						transform: "translateX(-40px)",
						backgroundColor: colors.tooltipBg,
						color: colors.tooltipText,
					}}
				>
					{tooltipTitle}
				</span>
			)}
			<i
				className={`${
					loading ? "tw-animate-spin " : ""
				} material-icons tw-text-3xl`}
				style={{ color: colors.icons }}
			>
				{loading ? "rotate_right" : name}
			</i>
		</button>
	);
};

interface IPlayerLayerProps {
	hideDocumentation?: boolean;
}

const PlayerLayer: React.FC<IPlayerLayerProps> = ({ hideDocumentation }) => {
	const dispatch = useAppDispatch();
	const fullscreen = useSelector(getFullscreen);
	const view = useSelector(getView);
	const playerLaoded = useSelector(getPlayerLoaded);
	const { arLoading, documentationLoading, snapshotLoading } =
		useSelector(getModes);
	const playerContext = useContext(PlayerContext);

	const isIOS = /Mobi|iPad|iPhone|iPod/i.test(navigator.userAgent);

	if (!playerLaoded) return null;

	const player = playerContext?.player;
	const translates = player?.mediator.translate("button") ?? {};

	const downloadDocumentation = () => {
		if (documentationLoading) return;

		dispatch(setDocumentationLoading(true));

		try {
			player.documentation
				.getSetDocumentations(player.setRoots)
				.catch(() => dispatch(setDocumentationLoading(false)))
				.then(() => dispatch(setDocumentationLoading(false)));
		} catch (e) {
			console.error(e);
			dispatch(setDocumentationLoading(false));
		}
	};

	const downloadSnapshot = () => {
		if (snapshotLoading) return;

		dispatch(setSnapshotLoading(true));

		try {
			playerContext.webComponent
				.takeASnapshot()
				.catch(() => dispatch(setSnapshotLoading(false)))
				.then(({ url }: any) => {
					window.open(url, "_blank");
					dispatch(setSnapshotLoading(false));
				});
		} catch (e) {
			console.error(e);
			dispatch(setSnapshotLoading(false));
		}
	};

	const downloadAr = () => {
		if (arLoading) return;

		dispatch(setArLoading(true));

		try {
			playerContext.webComponent
				.getGLTF()
				.catch(() => dispatch(setArLoading(false)))
				.then(() => dispatch(setArLoading(false)));
		} catch (e) {
			console.error(e);
			dispatch(setArLoading(false));
		}
	};

	return (
		<div>
			<PlayerLayerIcon
				name={view === "edit" ? "palette" : "arrow_back"}
				styles={`${
					fullscreen ? "tw-opacity-0 tw-pointer-events-none" : ""
				} tw-transition-all tw-duration-500 tw-bottom-4 tw-right-4 md:tw-top-4 md:tw-bottom-[unset]`}
				onClick={() => dispatch(toggleView())}
			/>
			{!hideDocumentation && (
				<PlayerLayerIcon
					loading={documentationLoading}
					tooltipTitle={translates["documentation"] ?? "button.documentation"}
					name="description"
					styles={
						isIOS
							? "tw-top-28 tw-right-4 md:tw-bottom-28 md:tw-top-[unset]"
							: "tw-top-40 tw-right-4 md:tw-bottom-40 md:tw-top-[unset]"
					}
					onClick={downloadDocumentation}
				/>
			)}
			{!isIOS && (
				<PlayerLayerIcon
					loading={snapshotLoading}
					tooltipTitle={translates["snapshot"]}
					name="wallpaper"
					styles="tw-top-28 tw-right-4 md:tw-bottom-28 md:tw-top-[unset]"
					onClick={downloadSnapshot}
				/>
			)}
			<PlayerLayerIcon
				loading={arLoading}
				tooltipTitle={translates["ar"]}
				name="view_in_ar"
				styles="tw-top-16 tw-right-4 md:tw-bottom-16 md:tw-top-[unset]"
				onClick={downloadAr}
			/>
			<PlayerLayerIcon
				tooltipTitle={
					!fullscreen
						? translates["fullscreen"] ?? "button.fullscreen"
						: translates["exit_fullscreen"] ?? "button.exit_fullscreen"
				}
				name={fullscreen ? "fullscreen_exit" : "fullscreen"}
				styles="tw-top-4 tw-right-4 md:tw-bottom-4 md:tw-top-[unset]"
				onClick={() => dispatch(toggleFullscreen())}
			/>
		</div>
	);
};

export default PlayerLayer;
