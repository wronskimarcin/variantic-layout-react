export interface IParameter {
    code: string;
    name: string;
    settings: any;
    type: string;
    unit: string;
    unitLabel: string;
    value: number;
    valueLabel: string;
    visible: boolean;
}