import { useSelector } from "react-redux";
import {
	getUnit,
	getVisibleCategoriesWithParameters,
} from "../../redux/configurationSlice";
import Category from "./Category";
import UnitPicker from "./UnitPicker";

const Configurationlayout = () => {
	const categories = useSelector(getVisibleCategoriesWithParameters);
	const selectedUnit = useSelector(getUnit);

	return (
		<>
			<UnitPicker />
			{categories.map((c, i) => (
				<Category key={`${selectedUnit}-${c.id}-${i}`} category={c} />
			))}
		</>
	);
};

export default Configurationlayout;
